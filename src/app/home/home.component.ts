import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UpdateAttoreComponent } from '../update-attore/update-attore.component';

import { AttoreService } from '../service/attore.service';

import { Attore } from './../entity/Attore';
import Nota from '../entity/Nota';
import Country from '../entity/Country';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  result: any;
  keys: any;
  page = 0;
  ascdesc = 'asc';
  size = 10;
  field = 'codAttore';
  router: Router;
  isLoaded = false;
  attoriService: AttoreService;

  constructor(router: Router, as: AttoreService) {
    this.router = router;
    this.attoriService = as;
  }

  ngOnInit(): void { }

  visualizza(): void {
    const s = `?page=${this.page}&size=${this.size}&ascdesc=${this.ascdesc}&field=${this.field}`;
    this.attoriService.getAttori(s)
      .subscribe((attori: object): void => {
        this.result = (attori as Array<Attore>).map(el => {
          const obj: any = { ...el };
          obj.nota = (obj.nota as Nota)?.note;
          obj.country = (obj.country as Country)?.country;
          return obj;
        });
        this.keys = Object.keys(this.result[0]);
        this.isLoaded = true;
      });
  }

  clear(): void {
    console.log('Clear');
    UpdateAttoreComponent.prototype.clear();
  }

  performDelete(id: number): void {
    if (id) {
      this.attoriService.deleteAttore(id);
      this.visualizza();
    }
  }

  performUpdate(id: number): void {

    UpdateAttoreComponent.prototype.getInfoAttore(id, this.attoriService);
    // this.router.navigate(['/putAttore']).then(() => {});
  }



}
