export default class Country {
    public country: string;

    constructor(country: string) {
        this.country = country;
    }
}
