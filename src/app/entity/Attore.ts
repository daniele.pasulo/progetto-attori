import Country from './Country';
import Nota from './Nota';

export class Attore {
    // Attributi
    codAttore: number | undefined;
    nome: string;
    annoNascita: Date;
    country: Country;
    nota: Nota | object | undefined;
    constructor(codAttore: number | undefined, nome: string, annoNascita: Date, country: Country, nota: Nota | object | undefined) {
        this.codAttore = codAttore;
        this.nome = nome;
        this.annoNascita = annoNascita;
        this.country = country;
        this.nota = nota;
    }
}
