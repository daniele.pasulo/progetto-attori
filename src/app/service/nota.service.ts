import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import HTTP_CONSTS from '../consts/HTTP_CONSTANTS';

@Injectable({
    providedIn: 'root'
})
export default class NotaService {

    constructor(private http: HttpClient) {}

    getNote(): Observable<any> {
        return this.http.get(`${HTTP_CONSTS.server}/nota`, HTTP_CONSTS.options);
    }
}
