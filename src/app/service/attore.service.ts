import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';

import HTTP from '../consts/HTTP_CONSTANTS';

import { Attore } from '../entity/Attore';
import Nota from '../entity/Nota';

@Injectable({
  providedIn: 'root'
})
export class AttoreService {
  host = HTTP.server + '/';
  options = HTTP.options;

  constructor(private http: HttpClient) {

  }

  form: FormGroup = new FormGroup({
    nome: new FormControl('', Validators.required),
    annoNascita: new FormControl(1990),
    country: new FormControl('Italy'),
    nota: new FormControl('')
  });

  initializeFormGroup(attore: Attore): void {
    console.log(attore);
    this.form.setValue({
      nome: attore.nome,
      annoNascita: attore.annoNascita,
      country: attore.country?.country,
      nota: (attore.nota as Nota)?.id || ''
    });
  }

  clearForm(): void {
    this.form.setValue({
      nome: '',
      annoNascita: '',
      country: '',
      note: ''
    });
  }

  getAttori(s: any): Observable<object> {
    return this.http.get(`${this.host}api/attori/${s}`, this.options);
  }

  getAttoriByID(codAttore: any): Observable<object> {
    return this.http.get(`${this.host}api/attori/${codAttore}`, this.options);
  }

  deleteAttore(codAttore: any): Subscription {
    console.log(codAttore);
    return this.http.delete(`${this.host}api/attori/${codAttore}`, this.options)
      .subscribe(() => console.log('Delete successful'));
  }

  addAttore(attore: Attore): Subscription {
    console.log(attore);
    const attoredb = new Attore(undefined, attore.nome, attore.annoNascita, attore.country, attore.nota);
    console.log(attoredb);
    return this.http.post(`${this.host}api/attori/`, attoredb, this.options).subscribe((val: any) => console.log('Messaggio inviato'));
  }

  modificaAttore(attore: Attore): Observable<object> {
    const attoredb = new Attore(attore.codAttore, attore.nome, attore.annoNascita, attore.country, attore.nota);
    console.log(attoredb);
    return this.http.put(`${this.host}api/attori/${attore.codAttore}`, attoredb, this.options);
  }
}
