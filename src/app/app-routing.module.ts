import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InserimentoAttoreComponent } from './inserimento-attore/inserimento-attore.component';
import { UpdateAttoreComponent } from './update-attore/update-attore.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'postAttore', component: InserimentoAttoreComponent },
  { path: 'putAttore', component: UpdateAttoreComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
