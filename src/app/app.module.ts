import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InserimentoAttoreComponent } from './inserimento-attore/inserimento-attore.component';
import { UpdateAttoreComponent } from './update-attore/update-attore.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InserimentoAttoreComponent,
    UpdateAttoreComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
