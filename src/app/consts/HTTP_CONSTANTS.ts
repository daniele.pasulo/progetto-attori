import { HttpHeaders } from '@angular/common/http';

export default {
    options: { headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*') },
    server: 'http://localhost'
};
