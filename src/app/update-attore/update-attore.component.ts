import { Component, OnInit } from '@angular/core';

import { AttoreService } from '../service/attore.service';
import NotaService from '../service/nota.service';

import { Attore } from '../entity/Attore';
import Nota from '../entity/Nota';


@Component({
  selector: 'app-update-attore',
  templateUrl: './update-attore.component.html',
  styleUrls: ['./update-attore.component.css']
})
export class UpdateAttoreComponent implements OnInit {

  attore: Attore | undefined;
  options: Nota[] = [];

  getInfoAttore(id: number, attoriService: AttoreService): void {
    attoriService.getAttoriByID(id).subscribe(data => {
      this.attore = data as Attore;
      attoriService.initializeFormGroup(this.attore as Attore);
    });
  }

  clear(): void {
    this.attore = undefined;
  }

  constructor(public attoriService: AttoreService, private notaService: NotaService) {
  }

  ngOnInit(): void {
    this.notaService.getNote().subscribe(data => {
      this.options = data;
    });
  }

  onSubmit(): void {
    const attore = this.attoriService.form.value;
    attore.codAttore = this.attore?.codAttore;
    console.log(attore);
    attore.nota = this.options.find(el => el.id === +attore.nota);
    this.attoriService.modificaAttore(attore).subscribe(() => {
      console.log('Modificato');
      this.clear();
    });
  }
}
