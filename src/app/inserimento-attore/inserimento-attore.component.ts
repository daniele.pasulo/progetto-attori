import { Component, OnInit } from '@angular/core';
import Country from '../entity/Country';
import Nota from '../entity/Nota';
import NotaService from '../service/nota.service';

import { Attore } from './../entity/Attore';
import { AttoreService } from './../service/attore.service';

@Component({
  selector: 'app-inserimento-attore',
  templateUrl: './inserimento-attore.component.html',
  styleUrls: ['./inserimento-attore.component.css']
})
export class InserimentoAttoreComponent implements OnInit {


  public options: Nota[] = [];

  constructor(public attoriService: AttoreService, private notaSercive: NotaService) { }

  ngOnInit(): void {
    this.notaSercive.getNote().subscribe(data => {
      this.options = data;
    });
  }

  onSubmit(): void {
    const formVal = this.attoriService.form.value;
    console.log(formVal);
    const nota = this.options.find(el => el.id === +formVal.id);
    const attore = new Attore(undefined, formVal.nome, formVal.annoNascita, new Country(formVal.country), nota);
    this.attoriService.addAttore(attore);
  }

}
