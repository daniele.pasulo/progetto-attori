import { Component } from '@angular/core';

import { UpdateAttoreComponent } from './update-attore/update-attore.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'attori';

  onClick(): void {
    UpdateAttoreComponent.prototype.clear();
  }
}
